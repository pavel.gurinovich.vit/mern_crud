export interface IState{
    products: IProducts
}

export interface IProduct {
    _id: string,
    name: string,
    price: string
}
export interface IProducts {
    data: Array<string>,
    loading: boolean,
    error: boolean
}

export interface ProductProps {
    products: IProducts,
    fetchCreateProduct: Function, 
    fetchGetProductsList: Function, 
    fetchEditProduct: Function, 
    fetchDeleteProduct: Function
}