import {useState} from 'react';
import {connect} from "react-redux";
import {fetchCreateProduct, fetchGetProductsList, fetchEditProduct, fetchDeleteProduct} from "../../store/actionCreators/product.action";
import { useEffect } from 'react';
import {IState, IProduct, ProductProps} from './product.interface'

const Product = (
    {   
        products, 
        fetchCreateProduct, 
        fetchGetProductsList, 
        fetchEditProduct, 
        fetchDeleteProduct
    }: ProductProps) =>{
 

    const [name, setName] = useState('');   
    const [isEdit, setIsEdit] = useState(false);
    const [id, setId] = useState('')
    const [price, setPrice] = useState('') 
   

    const handleCreate = async () =>{
        let product = {
            name: name ,
            price: price
        }
        let productPrice: number  = +price  
        if(name && productPrice){
            if(isEdit){
                let data = {
                    product,
                    id
                }
                fetchEditProduct(data)
            }else{
                
                fetchCreateProduct(product)
            }
          
        }
      
        setName('')
        setPrice('')
        setIsEdit(false)
    }
    useEffect(()=>{
        fetchGetProductsList()
      
    }, [])

    const handleEdit = async (product: IProduct) => {    
        setIsEdit(true)
        setName(product.name)
        setPrice(product.price)
        setId(product._id)        
    }

    const handleDelete = (id: string) => {
        fetchDeleteProduct(id);
        
    }

    return (
        <div className="admin">
            <div >
                <h2>Добавить товар:</h2>
                <input type="text" name="name" placeholder={'Product'} value={name} onChange={(e)=>setName(e.target.value)}/>
                <input type="text" name="price" placeholder={'Link'} value={price} onChange={(e)=>setPrice(e.target.value)}/>
                <button onClick={()=>handleCreate()}>Подтвердить</button>
            </div>

            {products.loading ? 'Loading': 
            products.error ? 'Error, try again':
            products.data.map((product: any) =>(
                
                    <div  key={product.id}>
                        <div >{product.name}</div>
                        <div >{product.price}</div>
                      
                        <div>
                            <button onClick={() => handleEdit(product)}>Изменить</button>
                            <button onClick={() => handleDelete(product._id)}>Удалить</button>
                        </div>
                    </div>
                ))
            }

        </div>
    );
}

const mapStateToProps = (state: IState) =>({
    products: state.products,
})



export default connect(mapStateToProps, 
    {fetchCreateProduct, fetchGetProductsList, fetchEditProduct, fetchDeleteProduct})
    (Product);
