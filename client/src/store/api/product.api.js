export const getProductsListAPI = async () =>{
    return fetch('api/product/getall', {
        method: "GET",
        headers: {
        "Content-type": "application/json; charset = UTF-8",
        },
    }).then(res => res.json())
}

export const createProductAPI = async (action) =>{
    return fetch('api/product/create', {
        method: "POST",
        body: JSON.stringify(action.data),
        headers: {
        "Content-type": "application/json; charset = UTF-8",
        },
    })
}

export const editProductAPI = async (action) =>{
    return fetch(`api/product/${action.id}`, {
        method: "PUT",
        body: JSON.stringify(action.product),
        headers: {
        "Content-type": "application/json; charset = UTF-8",
        },
    }).then(res=> res.json())
}

export const deleteProductAPI = async (id) =>{
    return fetch(`api/product/${id}`, {
        method: "DELETE",
        headers: {
        "Content-type": "application/json; charset = UTF-8",
        },
    }).then(res=> res.json())
}
