import { takeLatest, call, put } from "redux-saga/effects";
import { FETCH_CREATE_PRODUCT, FETCH_GET_PRODUCTS_LIST, FETCH_EDIT_PRODUCT, FETCH_DELETE_PRODUCT } from "../constants/product.constants";
import { createProductAPI, getProductsListAPI, editProductAPI, deleteProductAPI } from "../api/product.api";
import { 
    createProduct, createProductSuccess, createProductFailure,
    getProductsList, getProductsListSuccess, getProductsListFailure,
    editProduct, editProductSuccess, editProductFailure,
    deleteProduct, deleteProductSuccess, deleteProductFailure
 } from "../actionCreators/product.action";


export function* productsSagaWatcher (){
    yield takeLatest(FETCH_CREATE_PRODUCT, createProductFlow)
    yield takeLatest(FETCH_GET_PRODUCTS_LIST, getProductsListFlow)
    yield takeLatest(FETCH_EDIT_PRODUCT, editProductFlow)
    yield takeLatest(FETCH_DELETE_PRODUCT, deleteProductFlow)
}


function* createProductFlow(action){
    try{
        yield put(createProduct())
        
        yield call(() => createProductAPI(action))
        yield put(createProductSuccess(action.data))
    }catch (e){
        yield put(createProductFailure())
    }
}

function* getProductsListFlow(){
    try{
        yield put(getProductsList())

        let data = yield call(() => getProductsListAPI())
        yield put(getProductsListSuccess(data.data))
    }catch (e){
        yield put(getProductsListFailure())
    }
}


function* editProductFlow(action){
    try{
        yield put(editProduct())
        let data = yield call(() => editProductAPI(action.data))
        yield put(editProductSuccess(data.data))
    }catch (e){
        yield put(editProductFailure())
    }
}

function* deleteProductFlow(action){
    try{
        yield put(deleteProduct())
        let data = yield call(() => deleteProductAPI(action.id))
       
        yield put(deleteProductSuccess(data.data))
    }catch (e){
        yield put(deleteProductFailure())
    }
}































