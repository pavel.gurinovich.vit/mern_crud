import {fork} from 'redux-saga/effects'
import {productsSagaWatcher} from './productsSaga'
export default function* rootSaga() {
   yield fork(productsSagaWatcher)  
}