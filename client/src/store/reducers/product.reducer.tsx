
import {  
    CREATE_PRODUCT, CREATE_PRODUCT_SUCCESS, CREATE_PRODUCT_FAILURE, 
    GET_PRODUCTS_LIST, GET_PRODUCTS_LIST_SUCCESS, GET_PRODUCTS_LIST_FAILURE,
    EDIT_PRODUCT, EDIT_PRODUCT_SUCCESS, EDIT_PRODUCT_FAILURE,
    DELETE_PRODUCT, DELETE_PRODUCT_SUCCESS, DELETE_PRODUCT_FAILURE,
} from "../constants/product.constants";

import { IAction } from "./product.interface";

const initialState = {
    data: [],
    loading: false,
    error: false
}

export const productReducer = (state = initialState, action: IAction)=>{
    switch(action.type){
        case CREATE_PRODUCT:
            return {
                data: [...state.data],
                loading: true,
                error: false
            }
        case CREATE_PRODUCT_SUCCESS:
            return {
                data: [...state.data, action.data],
                loading: false,
                error: false
            }
        case CREATE_PRODUCT_FAILURE:
            return {
                data: [],
                loading: false,
                error: true
            }
        case GET_PRODUCTS_LIST:
            return{
                data: [],
                loading: true,
                error: false,        
            }
        case GET_PRODUCTS_LIST_SUCCESS:
            return {
                data: [...action.data],
                loading: false,
                error: false,
              };
        case GET_PRODUCTS_LIST_FAILURE:
            return {
                data: [],
                loading: false,
                error: true,
              };
        case EDIT_PRODUCT:
            return{
                data: [...state.data],
                loading: true,
                error: false,        
            }
        case EDIT_PRODUCT_SUCCESS:
            return {
                data: [...action.data],
                loading: false,
                error: false,
                };
        case EDIT_PRODUCT_FAILURE:
            return {
                data: [],
                loading: false,
                error: true,
                };
        case DELETE_PRODUCT:
            return{
                data: [...state.data],
                loading: true,
                error: false,        
            }
        case DELETE_PRODUCT_SUCCESS:
            return {
                data: [...action.data],
                loading: false,
                error: false,
                };
        case DELETE_PRODUCT_FAILURE:
            return {
                data: [],
                loading: false,
                error: true,
                };
        
        default: return state;
    }
}