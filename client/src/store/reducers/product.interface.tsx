export interface IAction {
    type: string,
    data: Array<string>
}
