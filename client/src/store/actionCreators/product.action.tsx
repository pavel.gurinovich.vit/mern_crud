import {
    CREATE_PRODUCT, CREATE_PRODUCT_SUCCESS, CREATE_PRODUCT_FAILURE, 
    GET_PRODUCTS_LIST, GET_PRODUCTS_LIST_SUCCESS, GET_PRODUCTS_LIST_FAILURE,
    EDIT_PRODUCT, EDIT_PRODUCT_SUCCESS, EDIT_PRODUCT_FAILURE,
    DELETE_PRODUCT, DELETE_PRODUCT_SUCCESS, DELETE_PRODUCT_FAILURE,
    FETCH_CREATE_PRODUCT, FETCH_GET_PRODUCTS_LIST, FETCH_EDIT_PRODUCT,FETCH_DELETE_PRODUCT
} from "../constants/product.constants";
import { IData, ICreate, IEdit } from "./product.action.interface";


export const createProduct = () => ({type: CREATE_PRODUCT});
export const createProductSuccess = (data: IData) => ({type: CREATE_PRODUCT_SUCCESS, data});
export const createProductFailure = () => ({type: CREATE_PRODUCT_FAILURE});

export const getProductsList = () => ({type: GET_PRODUCTS_LIST})
export const getProductsListSuccess = (data: IData) => ({type: GET_PRODUCTS_LIST_SUCCESS, data})
export const getProductsListFailure = () => ({type: GET_PRODUCTS_LIST_FAILURE})

export const editProduct = () => ({type: EDIT_PRODUCT});
export const editProductSuccess = (data: IData) => ({type: EDIT_PRODUCT_SUCCESS, data});
export const editProductFailure = () => ({type: EDIT_PRODUCT_FAILURE});

export const deleteProduct = () => ({type: DELETE_PRODUCT});
export const deleteProductSuccess = (data: IData) => ({type: DELETE_PRODUCT_SUCCESS, data});
export const deleteProductFailure = () => ({type: DELETE_PRODUCT_FAILURE});

export const fetchCreateProduct = (data: ICreate) => ({type: FETCH_CREATE_PRODUCT, data})
export const fetchGetProductsList = () => ({type: FETCH_GET_PRODUCTS_LIST})
export const fetchEditProduct = (data: IEdit) => ({type: FETCH_EDIT_PRODUCT, data})
export const fetchDeleteProduct = (id: string) => ({type: FETCH_DELETE_PRODUCT, id})