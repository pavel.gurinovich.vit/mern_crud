export interface IData {
    name: string,
    price: string
}

export interface ICreate {
    name: string,
    price: string
}

export interface IEdit {
    product: ICreate,
    id: string
}
