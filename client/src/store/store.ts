import { productReducer } from './reducers/product.reducer';
import { applyMiddleware, combineReducers, createStore } from "redux";
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/rootSaga';


const sagaMiddleware = createSagaMiddleware()

const rootReducer = combineReducers({
    products: productReducer
})

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(rootSaga)