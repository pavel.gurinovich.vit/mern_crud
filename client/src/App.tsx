import './App.css';
import {BrowserRouter, Route} from "react-router-dom"

import Product from './components/Product/Product';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Route path={'/'} render={()=><Product/>}/>
      </BrowserRouter>
    
    </div>
  );
}

export default App;
