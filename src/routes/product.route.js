import express from 'express';
import { create_product, get_all_products,delete_product, edit_product } from '../controllers/product.controller.js';
const router = express.Router()

router.post("/create", create_product);    
router.delete("/:id", delete_product);
router.put("/:id",  edit_product);
router.get("/getall", get_all_products);

export default router;