import pkg from 'mongoose'
const {Schema, model, Types} = pkg;


const schema = new Schema({
    name: {type: String, required: true, unique: true},
    price: {type: Number, required: true},
});

export default model('Product', schema);