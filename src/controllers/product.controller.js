import Product from '../models/Product.js'

export const create_product = async(req, res) =>{
    try{
        const {name, price} = req.body

        const isProduct = await Product.findOne({name});
        if(isProduct){
            return res.status(300).json({message: "Данный продукт уже существует"});
        }
        const product = new Product({name, price})

        await product.save()
        res.status(201).json({message: "Продукт создан"})
    }catch (e){
        res.status(400).json({message: "Ошибка создания продукта"})
    }
}


export const edit_product = async(req, res) =>{
    try{
        const product = await Product.findOne({_id: req.params.id});
        if(!product){
            return res.status(400).json({message:"Нет такого продукта"})
        }
        const {name, price} = req.body;
        await Product.updateOne({_id: req.params.id}, {name, price}) 
        get_all_products(req, res)
    }catch (e){
        res.status(400).json({message: "Update product error -> " + e})
    } 
}

export const get_all_products = async (req, res) => {
    try{
        const data = await Product.find();

        res.json({data});   
    }catch (e){
        res.status(400).json({message: "Get Products list error -> " + e})
    }
}

export const delete_product = async(req, res) =>{
    try{
        const product = await Product.findOne({_id: req.params.id})
        if(product){
            Product.deleteOne({_id: req.params.id}, function(err, doc) {
                if (err) {
                    return res.status(400).json({message: "error"});
                }
                get_all_products(req, res)
            });
        }
        else{
            res.status(400).json({message:`Такого продукта не существует`});
        }
         
    }catch (e){
        res.status(400).json({message: "Deleting product error -> " + e})
    }
}
