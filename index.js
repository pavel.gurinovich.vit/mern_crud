import express from "express";
import db from './src/db/db.js';
import { PORT } from "./src/helpers/config.js";
import productRouter from "./src/routes/product.route.js";


const app = express();

app.use(express.json({extended:true}));
app.use('/api/product', productRouter);

const start = async() =>{
    try{
        await db
        app.listen(PORT, ()=>[
            console.log(`Server is running on port: ${PORT}`)
        ]);
    }catch (e){
        console.log(e);
    }
}

start();